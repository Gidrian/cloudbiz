To initialize the script you have to run:  

docker build -t cluster-apache-spark:3.0.2 .

This script will generate masters and workers

Then you'll be able to run: docker compose up
This will create the test cluster

To run a task execute this in the master container 

/opt/spark/bin/spark-submit --master spark://spark-master:7077 \
--jars /opt/spark-apps/postgresql-42.2.22.jar \
--driver-memory 1G \
--executor-memory 1G \
/opt/spark-apps/main.py