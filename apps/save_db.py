# Importing packages
import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StructField, StringType, IntegerType 
from pyspark.sql.types import ArrayType, DoubleType, BooleanType
from pyspark.sql.functions import col,array_contains
from pyspark.sql import Row

# Implementing CSV file in PySpark 
spark = SparkSession.builder.config("spark.jars", "/opt/spark/jars/postgresql-42.2.5.jar") \
	.master("local").appName("PySpark_Postgres_test").getOrCreate()

# Reading csv file
dataframe = spark.read.csv("../data/data.csv", header=True, inferSchema =True)
print("####################################################################################################")
dataframe.printSchema()
dataframe.withColumn("id",col("id").cast("int"))
dataframe.show()
print("####################################################################################################")



dataframe.select("id","name","last_name").write.format("jdbc") \
    .option("url", "jdbc:postgresql://db:5432/bigdata") \
    .option("driver", "org.postgresql.Driver").option("dbtable", "person") \
    .option("user", "admin").option("password", "admin").mode("append").save()
